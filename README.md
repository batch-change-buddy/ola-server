# OLA Server

[![pipeline status](https://gitlab.com/mxklb/ola-server/badges/master/pipeline.svg)](https://gitlab.com/mxklb/ola-server/-/commits/master)

This is a docker container running an ola server: https://www.openlighting.org/ola.

It's a customized version of https://github.com/jakewright/docker-ola. It updates
the base image to *ubuntu:20.04* and adds support for *osc*, *usbpro* & *libusb*,
*artnet* & *e131* devices. Hardware devices connected to the docker host must be
accessible by the container. To do so mount individual devices like */dev/ttyUSB0*
into the container.

To start this ola server execute:

    docker-compose up
